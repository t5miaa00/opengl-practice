// vim: set colorcolumn=72:
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include "lib/util.h"

#define STB_IMAGE_IMPLEMENTATION
#include "lib/stb/stb_image.h"

static struct {
	GLuint vertex_buffer, element_buffer;
	GLuint textures[2];

	GLuint vertex_shader, fragment_shader, program;

	struct {
		GLint fade_factor;
		GLint textures[2];
	} uniforms;

	struct {
		GLint position;
	} attributes;

	GLfloat fade_factor;
} g_resources;

static void show_info_log(
		GLuint object,
		PFNGLGETSHADERIVPROC glGet__iv,
		PFNGLGETSHADERINFOLOGPROC glGet__InfoLog) {
	GLint log_length;
	char* log;

	glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);
	log = malloc(log_length); // Allocate memory for the log.
	glGet__InfoLog(object, log_length, NULL, log);
	fprintf(stderr, "show_info_log(): %s\n", log);
	free(log); // Free the memory from the log.
}

static GLuint make_buffer(
		GLenum target,
		const void *buffer_data,
		GLsizei buffer_size ) {
	GLuint buffer;

	glGenBuffers(1, &buffer);
	glBindBuffer(target, buffer);
	glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);
	
	return buffer;
}

static GLuint make_texture(const char *filename) {
	GLuint texture;
	int width, height;
	// flip the image veritcally (required for pngs)
	stbi_set_flip_vertically_on_load(1);
	stbi_uc *image = stbi_load(
			filename,
			&width, &height, 0,
			STBI_rgb);
	
	if (!image)
		return 0;
	

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(
			GL_TEXTURE_2D,
			GL_TEXTURE_MIN_FILTER,
			GL_LINEAR);
	glTexParameteri(
			GL_TEXTURE_2D,
			GL_TEXTURE_MAG_FILTER,
			GL_LINEAR);
	glTexParameteri(
			GL_TEXTURE_2D,
			GL_TEXTURE_WRAP_S,
			GL_CLAMP_TO_EDGE);
	glTexParameteri(
			GL_TEXTURE_2D,
			GL_TEXTURE_WRAP_T,
			GL_CLAMP_TO_EDGE);

	glTexImage2D(
			GL_TEXTURE_2D, 0, /*type, level of detail*/
			GL_RGB8, /*internal format*/
			width, height, 0, /*width, heigth, border*/
			GL_RGB, /*external format*/
			GL_UNSIGNED_BYTE, /*type*/
			image /*image data*/);

	stbi_image_free(image);

	return texture;
}

static GLuint make_shader(GLenum type, const char *filename) {
	GLint length;
	GLchar *source = file_contents(filename, &length);
	GLuint shader;
	GLint shader_ok;

	// If there are no sources
	if (!source) return 0;

	// Apply and compile the shader
	shader = glCreateShader(type);
	glShaderSource(shader, 1, (const GLchar**)&source, &length);
	free(source);
	glCompileShader(shader);

	glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
	if (!shader_ok) {
		fprintf(stderr, "Failed to compile %s:\n", filename);
		show_info_log(
				shader,
				glGetShaderiv,
				glGetShaderInfoLog);
		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

static GLuint make_program(
		GLuint vertex_shader,
		GLuint fragment_shader) {
	GLint program_ok;

	GLuint program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);

	// Check if program creation worked.
	glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
	if (!program_ok) {
		fprintf(stderr, "Failed to link shader program:\n");
		show_info_log(
				program,
				glGetProgramiv,
				glGetProgramInfoLog);
		glDeleteProgram(program);
		return 0;
	}

	return program;
}

// Vertex points
static const GLfloat g_vertex_buffer_data[] = {
	-1.0f, -1.0f,
	1.0f, -1.0f,
	-1.0f, 1.0f,
	1.0f, 1.0f
};
// Vertex point numbers
static const GLushort g_element_buffer_data[] = {0, 1, 2, 3};

static int make_resources(void) {
	// Vertex data
	g_resources.vertex_buffer = make_buffer(
			GL_ARRAY_BUFFER,
			g_vertex_buffer_data,
			sizeof(g_vertex_buffer_data));
	g_resources.element_buffer = make_buffer(
			GL_ELEMENT_ARRAY_BUFFER,
			g_element_buffer_data,
			sizeof(g_element_buffer_data));


	// Texture data
	g_resources.textures[0] = make_texture("src/texture1.png");
	g_resources.textures[1] = make_texture("src/texture2.png");
	
	if (g_resources.textures[0] == 0
			|| g_resources.textures[1] == 0) {
		return 0;
	}

	// Shader data
	g_resources.vertex_shader = make_shader(
			GL_VERTEX_SHADER,
			"src/shader.vert");
	if (g_resources.vertex_shader == 0)
		return 0;

	g_resources.fragment_shader = make_shader(
			GL_FRAGMENT_SHADER,
			"src/shader.frag");
	if (g_resources.fragment_shader == 0)
		return 0;

	// Shader program data
	g_resources.program = make_program(
			g_resources.vertex_shader,
			g_resources.fragment_shader);
	if (g_resources.program == 0)
		return 0;

	// Look up shader variable locations
	g_resources.uniforms.fade_factor = glGetUniformLocation(
			g_resources.program, "fade_factor");
	g_resources.uniforms.textures[0] = glGetUniformLocation(
			g_resources.program, "textures[0]"); 
	g_resources.uniforms.textures[1] = glGetUniformLocation(
			g_resources.program, "textures[1]");
	g_resources.attributes.position = glGetAttribLocation(
			g_resources.program, "position");

	return 1;
}

static void update_fade_factor(void) {
	int elapsed = glutGet(GLUT_ELAPSED_TIME); //in milliseconds

	g_resources.fade_factor =
		sinf((float)elapsed * 0.001f) * 0.5f + 0.5f;
	glutPostRedisplay();
}

static void render(void) {
	glUseProgram(g_resources.program);
	
	glUniform1f(
			g_resources.uniforms.fade_factor,
			g_resources.fade_factor);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, g_resources.textures[0]);
	glUniform1i(g_resources.uniforms.textures[0], 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, g_resources.textures[1]);
	glUniform1i(g_resources.uniforms.textures[1], 1);
	
	glBindBuffer(GL_ARRAY_BUFFER, g_resources.vertex_buffer);
	glVertexAttribPointer(
			g_resources.attributes.position, /*attribute*/
			2, /*size*/
			GL_FLOAT, /*type*/
			GL_FALSE, /*normalized?*/
			sizeof(GLfloat) * 2, /*stride*/
			(void*)0 /*array buffer offset*/);
	glEnableVertexAttribArray(g_resources.attributes.position);

	glBindBuffer(
			GL_ELEMENT_ARRAY_BUFFER,
			g_resources.element_buffer);
	glDrawElements(
			GL_TRIANGLE_STRIP, /*mode*/
			4, /*count*/
			GL_UNSIGNED_SHORT, /*type*/
			(void*)0 /*element array buffer offset*/);

	// Cleaning up after ourselves
	glDisableVertexAttribArray(g_resources.attributes.position);

	glutSwapBuffers();
}

int main(int argc, char** argv) {
	// GLUT initialization
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);

	// giving the window a size
	glutInitWindowSize(640, 480);

	// naming the window
	glutCreateWindow("OpenGL practise");
	
	// Rendering function and idle functions
	glutIdleFunc(&update_fade_factor);
	glutDisplayFunc(&render);

	// GLEW initialization
	glewInit();
	if (!GLEW_VERSION_2_0) {
		fprintf(stderr, "OpenGL 2.0 not avaivable!\n");
		return 1;
	}

	if (!make_resources()) {
		fprintf(stderr, "Failed to load resources!\n");
		return 1;
	}

	glutMainLoop();
	return 0;
}
