#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>

void *file_contents(const char *filename, GLint *length) {
	FILE *file = fopen(filename, "r");
	void *buffer;

	if (!file) {
		fprintf(stderr, "Unable to open %s for reading.", filename);
		return NULL;
	}

	fseek(file, 0, SEEK_END);
	*length = ftell(file);
	fseek(file, 0, SEEK_SET);

	buffer = malloc(*length + 1);
	*length = fread(buffer, 1, *length, file);
	fclose(file);

	((char*)buffer)[*length] = '\0';

	return buffer;
}
